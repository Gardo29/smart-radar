package launcher;

import javafx.application.Application;
import javafx.stage.Stage;
import view.ViewImpl;

public class Launcher extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(final Stage stage) throws Exception {
        new ViewImpl(stage);
    }
}
