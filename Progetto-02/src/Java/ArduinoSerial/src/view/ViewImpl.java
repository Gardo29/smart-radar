package view;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Paths;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import view.controller.ViewController;
import view.controller.ViewControllerImpl;
import view.screensettings.ScreenSettings;

public class ViewImpl implements View {

    private static final int WIDTH_PERCENT = 50;
    private static final int HEIGHT_PERCENT = 50;
    private static final String FXMLPath = "res/view.fxml";
    private static final String ERROR_MESSAGE = "Can't open the program";
    private static final Parent ROOT = new AnchorPane();
    private final ViewController viewController;
    private final Stage stage;

    public ViewImpl(final Stage stage) {
        this.stage = stage;
        this.viewController = new ViewControllerImpl(this.stage);
        this.createGui();
    }

    @Override
    public ViewController getController() {
        return this.viewController;
    }

    private void createGui() {
        /*
         * final FXMLLoader loader = new FXMLLoader(
         * ClassLoader.getSystemResource(FXMLPath));
         */
        FXMLLoader loader;
        try {
            loader = new FXMLLoader(Paths.get(FXMLPath).toUri().toURL());
            loader.setController(this.viewController);
            try {
                this.createScene(loader.load());
            } catch (IOException e) {
                e.printStackTrace();
                error(ERROR_MESSAGE);
            }
        } catch (MalformedURLException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

    }

    private void createScene(final Parent scene) {
        this.stage.show();
        this.stage.setScene(new Scene(ROOT));
        this.stage.setWidth(
                ScreenSettings.getSettings().getScaledWidth(WIDTH_PERCENT));
        this.stage.setHeight(
                ScreenSettings.getSettings().getScaledHeight(HEIGHT_PERCENT));
        this.stage.getScene().setRoot(scene);

    }

    private void error(final String message) {
        final Alert alert = new Alert(AlertType.ERROR);
        // close controller
        this.stage.hide();
        alert.setTitle("Error");
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();

    }
}
