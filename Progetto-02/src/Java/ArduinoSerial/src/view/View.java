package view;

import view.controller.ViewController;

public interface View {
    ViewController getController();
}
