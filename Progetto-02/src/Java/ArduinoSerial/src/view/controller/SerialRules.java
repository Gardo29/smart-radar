package view.controller;

import java.util.Optional;

import serial.SerialCommunication;

public class SerialRules {
	private static final String AUTO = "auto";
	private static final String MANUAL = "manual";
	private static final String SINGLE = "single";
	private static final byte AUTO_SERIAL = (byte) 183;
	private static final byte MANUAL_SERIAL = (byte) 182;
	private static final byte SINGLE_SERIAL = (byte) 181;
	
	private SerialRules() {
	}
	
	public static Optional<Byte> getConvertedData(final String data) {
		final String command = data.toLowerCase();
		if(command.equals(AUTO)) {
			return Optional.of(AUTO_SERIAL);
		}
		if(command.equals(MANUAL)) {
			return Optional.of(MANUAL_SERIAL);
		}
		if(command.equals(SINGLE)) {
			return Optional.of(SINGLE_SERIAL);
		}
		try {
			final int number = Integer.parseInt(command);
			if(number >= 0 && number <= 180) {
				return Optional.of((byte)number);
			}
			return Optional.empty();
		}catch (NumberFormatException e) {
			return Optional.empty();
		}
	}

}
