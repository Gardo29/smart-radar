package view.controller;

import java.net.URL;
import java.util.Observable;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.util.Pair;
import serial.SerialCommunication;

public class ViewControllerImpl implements ViewController,Initializable{
    private static final ObservableList<String> OPTION_SERIAL = FXCollections.observableArrayList("No Line Ending","Newline");
    private final Stage stage;
    private Optional<Pair<String,SerialCommunication>> serial;
    @FXML
    private CheckBox scrollSwitch;
    @FXML
    private TextField data;
    @FXML
    private TextArea receivedData;
    @FXML
    private Label status;
    @FXML
    private ComboBox<String> availablePorts;
    @FXML
    private ComboBox<String> mode;
    @FXML
    private Button disconnect;
    

        
    public ViewControllerImpl(final Stage stage) {
        this.stage = stage;
        this.serial = Optional.empty();
    }

    @Override
    public void update(Observable o, Object arg) {
        Platform.runLater(
                () -> this.addTextToArea(this.serial.get().getKey()));	
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.setItemsEnabled(false);
        this.addListeners();
        this.loadPorts();
        this.mode.setItems(OPTION_SERIAL);
        this.mode.getSelectionModel().selectFirst();
        this.scrollSwitch.setSelected(true);
    }
    @FXML
    private void writeData() {
        final String data = this.data.getText();
        if (!data.isEmpty()) {
        	final String endLine = this.mode.getValue().equals(OPTION_SERIAL.get(0)) ? "" : "\n";
            Optional<Byte> sendableData =SerialRules.getConvertedData(data+endLine);
            if(sendableData.isPresent()) {	
            	this.serial.get().getValue().writeData(sendableData.get());
            }
        }
    }

    private void addTextToArea(final String text) {
        final int caretPosition = receivedData.caretPositionProperty().get();
        this.receivedData.appendText(this.serial.get().getValue().getData());
        if (!this.scrollSwitch.isSelected()) {
            this.receivedData.positionCaret(caretPosition);
        }
    }

    private void setItemsEnabled(final boolean status) {
        this.data.setDisable(!status);
        this.disconnect.setDisable(!status);
    }

    @FXML
    private void loadPorts() {
        final ObservableList<String> connections = FXCollections
                .observableArrayList(SerialCommunication.getAvailablePorts());
        Platform.runLater(()->{
        	this.availablePorts.getItems().setAll(connections);
        	if(this.availablePorts.getItems().size()>0){
        		this.availablePorts.getSelectionModel().selectFirst();
        	}
        	});  
    }
    @FXML
    private void disconnect() {
    	this.serial.get().getValue().close();
    	this.setItemsEnabled(false);
    	this.serial = Optional.empty();
    }
    private void addListeners() {
        this.stage.setOnCloseRequest(e -> {
            if(this.serial.isPresent()){
            	this.serial.get().getValue().close();
            }
        });

        this.data.addEventHandler(KeyEvent.KEY_PRESSED, e -> {
            if (e.getCode().equals(KeyCode.ENTER)) {
                this.writeData();
                Platform.runLater(()->this.data.clear());
            }
        });
        this.availablePorts.setOnContextMenuRequested(e->{
        	this.loadPorts();
        });
        this.availablePorts.valueProperty().addListener(l -> {
        	final String currentPort = this.availablePorts.getValue();
        	if(!this.serial.isPresent() || !currentPort.equals(this.serial.get().getKey())) {
        		if(this.serial.isPresent()){
        			this.serial.get().getValue().close();
        		}
        		this.establishConnection(currentPort);
        		this.receivedData.clear();
        	}
        });
    }

    private void establishConnection(final String port) {
        this.serial = Optional.of(new Pair<>(port,new SerialCommunication(port)));
        if(this.serial.get().getValue().getPostStatus() != this.serial.get().getValue().BUSY) {
        	this.setItemsEnabled(true);
        	this.serial.get().getValue().addObserver(this);
        }
        this.status.setText(this.serial.get().getValue().getPostStatus());
        
    }
}
