package view.controller;

import java.util.Observer;

import javafx.fxml.Initializable;

public interface ViewController extends Observer, Initializable {

}
