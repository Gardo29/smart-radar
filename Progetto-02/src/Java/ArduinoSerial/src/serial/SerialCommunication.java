package serial;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Observable;
import java.util.Queue;
import java.util.stream.Collectors;
import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import jssc.SerialPortList;

public class SerialCommunication extends Observable
        implements
            SerialPortEventListener {

	
    private final SerialPort serialPort;
    private String portMessage;
    private Queue<String> availableMessages;
    private StringBuilder currentMessage;
    public final String BUSY = "Post busy";

    public SerialCommunication(final String portName) {
        this.portMessage = "Not opened yet";
        this.serialPort = new SerialPort(Objects.requireNonNull(portName));
        this.configurePort();
        this.currentMessage = new StringBuilder();
        this.availableMessages = new LinkedList<>();
    }
    @Override
    public synchronized void serialEvent(final SerialPortEvent event) {
        if(event.isRXCHAR() && event.getEventValue() > 0) {
            try {
            	final String received = serialPort
                        .readString(event.getEventValue());
            	this.currentMessage.append(received);
                if(received.contains("\n")) {
                	this.availableMessages.add(this.currentMessage.toString());
                	this.currentMessage = new StringBuilder();
                    this.setChanged();
                    this.notifyObservers();
                }
            } catch (SerialPortException ex) {
            	ex.printStackTrace();
            }
        }
    }

    public synchronized void writeData(final byte data) {
        try {
        	this.serialPort.writeByte(data);
        } catch (SerialPortException e) {
            e.printStackTrace();
        }
    }
    private void configurePort() {
        try {
            serialPort.openPort();
            serialPort.setParams(SerialPort.BAUDRATE_9600,
                    SerialPort.DATABITS_8, SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);

            serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_RTSCTS_IN
                    | SerialPort.FLOWCONTROL_RTSCTS_OUT);

            serialPort.addEventListener(this, SerialPort.MASK_RXCHAR);
            this.portMessage = this.serialPort.getPortName();
        } catch (SerialPortException ex) {
            this.portMessage = BUSY;
        }
    }
    public void close() {
        if (!this.serialPort.isOpened()) {
            return;
        }
        try {
            this.serialPort.removeEventListener();
            this.serialPort.closePort();

        } catch (SerialPortException e) {
            e.printStackTrace();
        }
    }

    public synchronized String getData(){
    	return this.availableMessages.poll();
    }
    public String getPostStatus() {
        return this.portMessage;
    }

    public static List<String> getAvailablePorts() {
        return Arrays.stream(SerialPortList.getPortNames())
                .collect(Collectors.toList());
    }
}
