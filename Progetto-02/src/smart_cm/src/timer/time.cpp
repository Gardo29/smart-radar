#include "time.h"

void Timer::setTimer(void (*func)(), unsigned long period) {
  MiniTimer1.init();
  MiniTimer1.stop();
  MiniTimer1.reset();
  MiniTimer1.setPeriod(period);
  MiniTimer1.attachInterrupt(func);
  MiniTimer1.start();
}

void Timer::setTimer(unsigned long period) {
  MiniTimer1.init();
  MiniTimer1.stop();
  MiniTimer1.reset();
  MiniTimer1.setPeriod(period);
  MiniTimer1.detachInterrupt();
  MiniTimer1.start();
}
