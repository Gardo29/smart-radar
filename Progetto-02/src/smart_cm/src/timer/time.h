#ifndef _TIME_H_
#define _TIME_H_
/*  Class for time management */

#include "Arduino.h"
#include "MiniTimerOne.h"

class Timer{
    private:
        Timer();
    public:
        /* start a timer which calls the param function on specified period*/
        static void setTimer(void (*func)(), unsigned long period);
        /* start a timer which creates a generic interrupt after the specified period*/
        static void setTimer(unsigned long period);
};
#endif
