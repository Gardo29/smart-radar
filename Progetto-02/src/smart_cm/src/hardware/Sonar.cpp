#include "Sonar.h"


Sonar::Sonar(unsigned int trigPin, unsigned int echoPin, unsigned int temperature,unsigned long timeout) {
  this->trigPin = trigPin;
  this->echoPin = echoPin;
  pinMode(this->trigPin,OUTPUT);
  pinMode(this->echoPin,INPUT);
  this->velocity = 331.45 + 0.62 * temperature;
  this->distance = Sonar::NOT_CALCULATED;
  this->timeout = timeout;
}
double Sonar::getDistanceMeters() {
  if(this->distance != Sonar::NOT_CALCULATED){
    double getDistance = this->distance;
    this->distance = Sonar::NOT_CALCULATED;
    return getDistance;
  }
  double travelTime = 0;
  digitalWrite(this->trigPin, LOW);
  delayMicroseconds(Sonar::DELAY_LOW_MICROSECONDS);
  digitalWrite(this->trigPin, HIGH);
  delayMicroseconds(Sonar::PULSE_ON_MICROSECONDS);
  digitalWrite(this->trigPin, LOW);

  travelTime = pulseIn(echoPin, HIGH,this->timeout) / 1000.0 / 1000.0; // /1000000 for meters
  return travelTime / 2 * this->velocity;
}
bool Sonar::objectIn(unsigned int distMin,unsigned int distMax){
  this->distance = this->getDistanceMeters();
  if(distMin < this->distance || this->distance < distMax){
    return true;
  }
  return false;
}
