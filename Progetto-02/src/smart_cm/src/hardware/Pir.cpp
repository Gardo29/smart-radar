#include "Pir.h"

Pir::Pir(unsigned int pin) {
    this->pin = pin;
    pinMode(this->pin,INPUT);
}
bool Pir::isDetecting() {
    return digitalRead(this->pin) == HIGH;
}
void Pir::interrupt(void (*func)(void),unsigned int mode){
    attachInterrupt(digitalPinToInterrupt(this->pin),func,mode);
}
void Pir::deInterrupt(void){
    detachInterrupt(digitalPinToInterrupt(this->pin));
}
void Pir::calibrate(unsigned int seconds){
    for(int i = 0 ;i < seconds; i++){
        delay(Pir::SECOND);
    }
}
