#ifndef _PIR_H_
#define _PIR_H_
/* It models a pir sensor, with methods for attaching/detaching interrupts 
or knowing if the sensor is detecting*/
#include "Arduino.h"

class Pir{
    private:
        static const unsigned int SECOND = 1000;
        unsigned int pin;
    public:
        Pir(unsigned int pin);
        bool isDetecting();
        void interrupt(void (*func)(void),unsigned int mode);
        void deInterrupt(void);
        void calibrate(unsigned int seconds);
};
#endif
