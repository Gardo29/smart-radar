#include "Button.h"

Button::Button(unsigned int pin){
    this->pin = pin;
    pinMode(this->pin,INPUT);
}
bool Button::isPressed(){
    return digitalRead(this->pin) == HIGH;
}
