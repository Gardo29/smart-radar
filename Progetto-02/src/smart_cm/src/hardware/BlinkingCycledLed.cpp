#include "BlinkingCycledLed.h"

BlinkingCycledLed::BlinkingCycledLed(unsigned int pin,unsigned int cyclesActive,unsigned int cyclesInactive):Led(pin){
    this->cyclesActive = cyclesActive;
    this->cyclesInactive = cyclesInactive;
    this->currentCycle = 0;
}
void BlinkingCycledLed::switchOn(){
    this->currentCycle++;
    switch(this->isActive()){
        case false:          
            if(this->currentCycle == this->cyclesInactive){
                Led::switchOn();
                this->currentCycle = 0;
            }
        case true:
            if(this->currentCycle == this->cyclesActive){
                this->switchOff();
                this->currentCycle = 0;
            }
    }
}
