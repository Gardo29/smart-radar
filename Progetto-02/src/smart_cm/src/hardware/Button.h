#ifndef _BUTTON_H_
#define _BUTTON_H_
/* It models a generic button, from which pression state can be obtained*/
#include "Arduino.h"

class Button{
    private:
        unsigned int pin;
    public:
        Button(unsigned int pin);
        bool isPressed();
};


#endif
