#ifndef _BLINKING_CYCLED_LED_H
#define _BLINKING_CYCLED_LED_H

/* It represents a blinking led managed by loop cycles for inactive and active state for the led*/


#include "Led.h"

class BlinkingCycledLed : public Led{
    private:
        unsigned int cyclesActive;
        unsigned int cyclesInactive;
        unsigned int currentCycle;
    public:
        BlinkingCycledLed(unsigned int pin,unsigned int cyclesActive,unsigned int cyclesInactive);
        void switchOn();
};

#endif
