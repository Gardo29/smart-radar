#ifndef _SONAR_H_
#define _SONAR_H_
/* It models a sonar, with methods for receive the current distance expressed in meter
or if an object is between a specified interval*/
#include "Arduino.h"

class Sonar{
    private:
        static constexpr double NOT_CALCULATED = -1.0;
        static const unsigned int DELAY_LOW_MICROSECONDS = 3;
        static const unsigned int PULSE_ON_MICROSECONDS = 5;
        unsigned int trigPin;
        unsigned int echoPin;
        double velocity;
        double distance;
        unsigned long timeout;
    public:
        Sonar(unsigned int trigPin,unsigned int echoPin,unsigned int temperature,unsigned long timeout);
        double getDistanceMeters();
        bool objectIn(unsigned int distMin,unsigned int distMax);
};

#endif
