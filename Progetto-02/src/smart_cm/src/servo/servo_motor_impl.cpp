#include "servo_motor_impl.h"
#include "Arduino.h"

ServoMotorImpl::ServoMotorImpl(int pin){
  this->pin = pin;
}

void ServoMotorImpl::on(){
  motor.attach(pin);
}

void ServoMotorImpl::setPosition(int angle){
  // 544 -> 0, 2400 -> 180
  // 544 + angle*(2400-544)/180
  float coeff = (2400.0-544.0)/180;
  motor.write(544 + angle*coeff);
}

void ServoMotorImpl::off(){
  motor.detach();
}
