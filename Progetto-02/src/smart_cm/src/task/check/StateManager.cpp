#include "StateManager.h"
StateManager::StateManager(Task* manualMode,Task* autoMode,Task* singleMode){
    this->manualMode = manualMode;
    this->autoMode = autoMode;
    this->singleMode = singleMode;
}
void StateManager::changeState(byte state){
    this->manualMode->setActive(false);
    this->autoMode->setActive(false);
    this->singleMode->setActive(false);

    switch(state){
        case AUTO:
            this->autoMode->init();
            this->autoMode->setActive(true);
        break;
        case MANUAL:
            this->manualMode->init();
            this->manualMode->setActive(true);
        break;
        case SINGLE:
            this->singleMode->init();
            this->singleMode->setActive(true);
        break;
    }
}
