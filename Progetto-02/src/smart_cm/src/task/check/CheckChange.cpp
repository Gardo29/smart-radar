#include "CheckChange.h"

CheckChange::CheckChange(byte activeState,StateManager* stateManager,Button* buttonAuto,Button* buttonManual,Button* buttonSingle){
    this->buttonAuto = buttonAuto;
    this->buttonManual = buttonManual;
    this->buttonSingle = buttonSingle;
    this->activeState = activeState;
    this->stateManager = stateManager;
}
byte CheckChange::checkButtons(){
    if(this->buttonAuto->isPressed()){
        return AUTO;
    }
    if(this->buttonManual->isPressed()){
        return MANUAL;
    }
    if(this->buttonSingle->isPressed()){
        return SINGLE;
    }
    return this->activeState;
}
void CheckChange::tick(){
    byte actual = this->checkButtons();
    if(SerialManager::isAvailable()){
        if(SerialManager::isChange()){
            actual = SerialManager::getData();
        }
    }
    if(this->activeState != actual){
        this->activeState = actual;
        this->stateManager->changeState(this->activeState);
    }
}
void CheckChange::init(){}
