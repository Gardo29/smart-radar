#ifndef _STATE_MANAGER_H_
#define _STATE_MANAGER_H_

#include "../Task.h"
#include "../../config.h"

/* It only has to manage the state tasks initialization or disactivation */
class StateManager{
    private:
        Task* manualMode;
        Task* autoMode;
        Task* singleMode;
    public:
        StateManager(Task* manualMode,Task* autoMode,Task* singleMode);
        void changeState(byte state);
};

#endif
