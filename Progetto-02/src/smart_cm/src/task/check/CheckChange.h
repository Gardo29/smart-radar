#ifndef _CHECK_CHANGE_H_
#define _CHECK_CHANGE_H_

#include "Arduino.h"
#include "../../serial/SerialManager.h"
#include "../../hardware/Button.h"
#include "../../config.h"
#include "StateManager.h"
/* Task which has to check if there is a state change from buttons or serial*/
class CheckChange : public Task {
    private:
        Button* buttonAuto;
        Button* buttonManual;
        Button* buttonSingle;
        StateManager* stateManager;
        byte activeState;
        byte checkButtons();
    public:
        CheckChange(byte activeState,StateManager* stateManager,Button* buttonAuto,Button* buttonManual,Button* buttonSingle);
        void tick();
        void init();
};



#endif
