#include "AutoMode.h"

AutoMode::AutoMode(Sonar* sonar,Led* ledA,Potentiometer* pot,ServoMotor* servo) {  
	this->action = new Action(servo); 
	this->sonar = sonar;
	this->ledA = ledA;
	this->pot = pot;
}
AutoMode::~AutoMode(){
	delete this->action;
}

void AutoMode::tick() {
		/*		distance->state check		*/
	double distance = this->sonar->getDistanceMeters();
	if(distance < DISTANCE_MAX){
		if(DISTANCE_MIN < distance){
			this->state = ALLARM;
		}
		else {
			this->state = TRACKING;
		}
		Utility::printAllarm();
		this->ledA->switchOn();
	}
	else{
		this->state = NORMAL;
		this->ledA->switchOff();
	}
	Utility::print(distance,this->action->getDegree());
	/*		moving check		*/
	if(!this->isMoving){
		unsigned int scanTime = this->pot->getMappedValue(TIME_SCAN_MIN,TIME_SCAN_MAX);
		if(SerialManager::isAvailable()){
			if(SerialManager::isTime()){
				scanTime = SerialManager::getData()*TO_MILLISEC;
			}
		}
		this->action->init(scanTime);
		this->isMoving = true;
	}
	if(this->state != TRACKING){
		this->action->move();
		if(this->action->getDegree() == 0){
			this->isMoving = false;
		}	
	}
}
void AutoMode::setActive(bool state){
	Task::setActive(state);
	if(state == false){
		this->ledA->switchOff();
	}
}
void AutoMode::init(){
	this->state = NORMAL;
	this->ledA->switchOff();
	this->isMoving = false;
}
