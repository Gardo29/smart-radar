#ifndef _AUTOMODE_H_
#define _AUTOMODE_H_
#include "../Utility.h"
#include "../Task.h" 
#include "../action/Action.h" 
#include "../../serial/SerialManager.h"
#include "../../hardware/Led.h" 
#include "../../hardware/Sonar.h" 
#include "../../hardware/Potentiometer.h"
#include "../../hardware/Pir.h"
#include "../../servo/servo_motor.h"
/* The main AutoMode */
class AutoMode: public Task{  
	private:
		Action* action;
		Sonar* sonar;
		Potentiometer* pot;
		Led* ledA;
		enum {ALLARM, TRACKING, NORMAL} state;
		bool isMoving;
	public:  
		AutoMode(Sonar* sonar,Led* ledA,Potentiometer* pot,ServoMotor* servo);
		~AutoMode();
		void setActive(bool state);
		void tick(); 
		void init();
}; 
#endif
