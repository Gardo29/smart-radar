#include "ManualMode.h"

ManualMode::ManualMode(Sonar* sonar,ServoMotor* servo) {  
	this->sonar = sonar;
	this->servo = servo;
	this->servoPosition = ManualMode::DEFAULT_SERVO;
 
}
void ManualMode::tick() {
	if(SerialManager::isAvailable()){
		if(SerialManager::isAngle()){
			this->servoPosition = SerialManager::getData();
			this->servo->setPosition(this->servoPosition);
		}
	}
	Utility::print(this->sonar->getDistanceMeters(),this->servoPosition);
}
void ManualMode::init() {
	this->servoPosition = ManualMode::DEFAULT_SERVO;
	this->servo->setPosition(this->servoPosition);
}
