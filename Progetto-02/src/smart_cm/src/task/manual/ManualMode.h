#ifndef _MANUALMODE_H_
#define _MANUALMODE_H_
#include "../Task.h"
#include "../../serial/SerialManager.h" 
#include "../../hardware/Sonar.h" 
#include "../../servo/servo_motor.h"
#include "../Utility.h"
/* Main ManualMode */
class ManualMode: public Task {  
	private:
		static const unsigned int DEFAULT_SERVO = 90;
		Sonar* sonar;
		ServoMotor* servo;
		unsigned int servoPosition;
	public:  
		ManualMode(Sonar* sonar,ServoMotor* servo);
		void tick(); 
		void init();
}; 
#endif
