#include "Utility.h"
Utility::Utility(){}
void Utility::print(double distance, unsigned int degree){
	if(distance > DISTANCE_MAX){
        SerialManager::writeData(String(degree));
	    //SerialManager::writeData(String(degree)+ELEMENT_NOT_PRESENT+String(distance));
    }
    else{
        SerialManager::writeData(String(degree)+ELEMENT_PRESENT+distance);
    }
}

void Utility::Utility::printAllarm(){
    SerialManager::writeData("ALLARM");
}

void Utility::idleMode(){
    set_sleep_mode(SLEEP_MODE_IDLE);
    sleep_enable();

    /* Disable all of the unused peripherals. This will reduce power
       consumption further and, more importantly, some of these
       peripherals may generate interrupts that will wake our Arduino from
       sleep!
    */
    power_adc_disable();
    power_spi_disable();
    power_timer0_disable();
    power_timer2_disable();
    power_twi_disable();
    /* Now enter sleep mode. */
    sleep_mode();
    /* The program will continue from here after the timer timeout*/
    sleep_disable(); /* First thing to do is disable sleep. */
    /* Re-enable the peripherals. */
    power_all_enable();
}
