#ifndef _SINGLEMODE_H_
#define _SINGLEMODE_H_
#include "../Task.h" 
#include "../action/Action.h"
#include "../Utility.h"
#include "../../serial/SerialManager.h"
#include "../../hardware/Led.h" 
#include "../../hardware/Sonar.h" 
#include "../../hardware/Potentiometer.h"
#include "../../hardware/Pir.h"
#include "../../servo/servo_motor.h"

/* main SingleMode 
If the pir doesnt detect anything the system goes repeatedly in and out of idle mode*/

class SingleMode: public Task{  
	private:
		Action* action;
		Sonar* sonar;
		Led* ledD;
		Potentiometer* pot;
		Pir* pir;
		bool hasDetected;
		void blinkLed(); 
	public:  
		SingleMode(Sonar* sonar,Led* ledD,Potentiometer* pot,Pir* pir,ServoMotor* servo);
		~SingleMode();
		void setActive(bool state);
		void tick();
		void init();
}; 
#endif
