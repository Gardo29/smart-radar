#include "SingleMode.h"

SingleMode::SingleMode(Sonar* sonar,Led* ledD,Potentiometer* pot,Pir* pir,ServoMotor* servo) { 
	this->action = new Action(servo); 
	this->sonar = sonar;
	this->ledD = ledD;
	this->pot = pot;
	this->pir = pir; 
}
SingleMode::~SingleMode(){
	delete this->action;
}
void SingleMode::tick() {
	if(!pir->isDetecting() && !this->hasDetected){
		this->ledD->switchOff();
		Utility::idleMode();
		return;
	}
	if(pir->isDetecting() && !this->hasDetected) {
		unsigned int scanTime = this->pot->getMappedValue(TIME_SCAN_MIN,TIME_SCAN_MAX);
		if(SerialManager::isAvailable()){
			if(SerialManager::isTime()){
				scanTime = SerialManager::getData()*TO_MILLISEC;
			}
		}
		this->action->init(scanTime);
		this->hasDetected = true;
	}
	if(this->hasDetected){
		this->action->move();
		if(this->action->getDegree() == 0){
			this->hasDetected = false;
		}
		double distance = this->sonar->getDistanceMeters();
		if(distance < DISTANCE_MAX){
			this->ledD->switchOn();
		}else{
			this->ledD->switchOff();
		}
		Utility::print(distance,this->action->getDegree());
	}
}
void SingleMode::setActive(bool state){
	Task::setActive(state);
	if(state == false){
		this->ledD->switchOff();
	}
}
void SingleMode::init(){
	this->ledD->switchOff();
	this->hasDetected = false;
}
