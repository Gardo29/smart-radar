#ifndef _TASK_H_ 
#define _TASK_H_
#include "../hardware/Sonar.h"
/* Abstract class for a generic task executable in the scheduler
Each tasks can be activated or disactivated or initialized
Tick method runs the specific tasks behavior*/
class Task { 
  private:
    bool active = false;
  public:  
    virtual void init() = 0;
    virtual void tick() = 0;
    virtual void setActive(bool state);
    virtual bool isActive();
}; 

#endif
