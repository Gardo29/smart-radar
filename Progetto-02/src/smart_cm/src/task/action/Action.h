#ifndef _ACTION_H_
#define _ACTION_H_
#include "../../servo/servo_motor_impl.h"
#include "../../task/Task.h"
#include "../../config.h"
#include "math.h"
/* Action manage a scan in all specified sectors in a specified time 
The scan is carried out in both directions
The wait time is obtained with void cicles of scheduler*/ 
class Action {  
	private:
        static const unsigned int START_POSITION = 0;
		ServoMotor* servo;
        unsigned int degree;
        int currentPos;
        unsigned int time;
        unsigned int nCycles;
        unsigned int currentCycle;
        unsigned int discard;
        unsigned int currentDisc;
        enum {RIGHT = 1,LEFT = -1} state;
        void startPosition();
        void updatePosition(); 
    public:  
		Action(ServoMotor* servo);
        double getDegree();
        void move();
        void scan();
        void init(unsigned int time);
}; 
#endif
