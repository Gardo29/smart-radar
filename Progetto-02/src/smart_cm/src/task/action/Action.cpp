#include "Action.h"

Action::Action(ServoMotor* servo) {  
	this->servo = servo;
}
void Action::init(unsigned int time){
    this->time = time;
    this->state = LEFT;
    this->currentPos = Action::START_POSITION;  
    this->nCycles = ceil(ceil(this->time/SECTIONS)/INTERRUPT_TIME)-1;
    this->discard = (this->time/SECTIONS)%INTERRUPT_TIME;
    this->currentDisc = 0;
    this->currentCycle = 0;
}
void Action::move() {
    if(this->currentCycle > 0){
        this->currentCycle--;
        return;
    }
    this->updatePosition();
    this->servo->setPosition(this->getDegree());
    this->currentCycle = this->nCycles;
    this->currentDisc += this->discard;

    if(this->currentCycle != 0 && (this->currentDisc/INTERRUPT_TIME) > 0){
        this->currentCycle--;
        this->currentDisc -= INTERRUPT_TIME;
    }
}
void Action::updatePosition() {
    if(this->currentPos == SECTIONS || this->currentPos == Action::START_POSITION){
        state = (state ==  RIGHT) ? LEFT : RIGHT;
    }
    this->currentPos += state;
}
double Action::getDegree(){
    return map(this->currentPos,Action::START_POSITION,SECTIONS,MIN_ANGLE,MAX_ANGLE);
}
