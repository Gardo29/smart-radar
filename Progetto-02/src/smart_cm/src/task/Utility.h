#ifndef _UTILITY_H_
#define _UTILITY_H_

#include "Arduino.h"
#include "../config.h"
#include "../serial/SerialManager.h"
#include <avr/sleep.h>
#include <avr/power.h>
/* utility functions prints and Idle mode*/
class Utility{
    private:
        Utility();
    public:
        static void print(double distance, unsigned int degree);
        static void printAllarm();
        static void idleMode();
};
#endif
