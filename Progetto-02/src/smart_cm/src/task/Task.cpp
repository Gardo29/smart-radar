#include "Task.h"

void Task::setActive(bool state){
  this->active = state;
}

bool Task::isActive(){
  return this->active;
}