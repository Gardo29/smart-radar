#include "SerialManager.h"

SerialManager::SerialManager(){}

bool SerialManager::isAvailable(){
    return Serial.available();
}
bool SerialManager::isAngle(){
    byte data = Serial.peek();
    if(MAX_ANGLE < data){
        return false;
    }
    return true;
}
bool SerialManager::isTime(){
    byte data = Serial.peek();
    if(TIME_SCAN_MIN < data || data < TIME_SCAN_MAX){
        return true;
    }
    if(SerialManager::isAngle()){
        SerialManager::getData();
    }
    return false;
}
bool SerialManager::isChange(){
    byte data = Serial.peek();
    if(data == AUTO || data == SINGLE || data == MANUAL){
        return true;
    }
    return false;
}
byte SerialManager::getData(){
    return Serial.read();
}

void SerialManager::writeData(String data){
    Serial.println(data);
}
