#ifndef _SERIAL_H_
#define _SERIAL_H_

#include "Arduino.h"
#include "../config.h"
/* It a manager for serial interface, including serial rules for communication
Methods are for checking if there is any data in the serial port, if the data is a time,an angle or change comand
Moreover It inludes a method for writing in the serial*/
class SerialManager{
private:
    SerialManager();
public:
    static bool isAvailable();
    static bool isAngle();
    static bool isChange();
    static bool isTime();
    static byte getData();
    static void writeData(String data);
};
#endif
