#ifndef __SCHEDULER__ 
#define __SCHEDULER__ 
#include "../task/Task.h" 
/* It models a simply scheduler with methods for update the scheduler itself, execute all available tasks
or add a new task*/
class Scheduler {    
	private:
		static const unsigned int MAX_TASKS = 10;
		unsigned short nTasks;  
		bool updated;
		Task* taskList[MAX_TASKS];    
	public:    
		Scheduler();  
		void update();
		virtual bool addTask(Task* task);  
		virtual void schedule();
};
#endif
