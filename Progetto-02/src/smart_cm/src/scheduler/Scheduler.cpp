#include "Scheduler.h"

#include "../servo/servo_motor_impl.h"

Scheduler::Scheduler() {     
	this->nTasks = 0; 
	this->updated = false;
} 
bool Scheduler::addTask(Task* task) {  
	if (this->nTasks < Scheduler::MAX_TASKS-1) {
		this->taskList[this->nTasks] = task;
		this->nTasks++;
		return true;
	} 
	else {
		return false; 
	}
}
void Scheduler::schedule() {
	if(this->updated){
		this->updated = false;
		for (int i = 0; i < this->nTasks; i++) {
			if(taskList[i]->isActive()){
				taskList[i]->tick();  
			}
		}
	}
}
void Scheduler::update(){
	this->updated = true;
}
