#ifndef _CONFIG_H_
#define  _CONFIG_H_
/* A list for constants for the project*/
#define SERIAL_SPEED 9600
/*      commands        */
#define MIN_ANGLE 0
#define MAX_ANGLE 180
#define SINGLE 181
#define MANUAL 182
#define AUTO 183

/*      pins        */
#define LED_A 6
#define LED_D 7
#define ECHO 9
#define TRIG 8
#define BUTTON_AUTO 3
#define BUTTON_MANUAL 4
#define BUTTON_SINGLE 5
#define PIR 11
#define SERVO 10
#define POTENTIOMETER A0

/*      time      */
#define SONAR_TIMEOUT 40000 // microseconds
#define PIR_CALIBRATION_TIME 10 // 10 SECONDS
#define TO_MILLISEC 1000L
#define TO_MICROSEC 1000L
#define INTERRUPT_TIME 125 //milliseconds
#define TIME_SCAN_MIN 2000 // milliseconds
#define TIME_SCAN_MAX 10000 // milliseconds

/*      sos     */
#define SECTIONS 16
#define TEMPERATURE 20 // °C

/*     blink led timers        */
#define CYCLES_ACTIVE 3
#define CYCLES_INACTIVE 3

/*      distances       */
#define DISTANCE_MIN 0.2
#define DISTANCE_MAX 0.4

/*      texts       */
#define ELEMENT_PRESENT " ELEMENT FOUND "
#define ELEMENT_NOT_PRESENT " ELEMENT NOT FOUND "

#endif
