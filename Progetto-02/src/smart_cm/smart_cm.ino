 /*    time    */
#include "src/timer/time.h"
/*    hardware    */
#include "src/hardware/Pir.h"
#include "src/servo/servo_motor_impl.h"
#include "src/hardware/Button.h"
#include "src/hardware/BlinkingCycledLed.h"
#include "src/hardware/Potentiometer.h"
#include "src/hardware/Sonar.h"
#include "src/hardware/Sonar.h"
/*    scheduler   */
#include "src/scheduler/Scheduler.h"
/*    tasks   */
#include "src/task/check/StateManager.h"
#include "src/task/check/CheckChange.h"
#include "src/task/auto/AutoMode.h"
#include "src/task/manual/ManualMode.h"
#include "src/task/single/SingleMode.h"
/*    config    */
#include "src/config.h"

Scheduler* scheduler = new Scheduler();

void setup() {
  /*    serial setup   */
  Serial.begin(SERIAL_SPEED);
  /*    hardware setup    */
  Button* buttonAuto = new Button(BUTTON_AUTO);
  Button* buttonManual = new Button(BUTTON_MANUAL);
  Button* buttonSingle = new Button(BUTTON_SINGLE);
  Pir* pir = new Pir(PIR);
  Led* ledA = new BlinkingCycledLed(LED_A, CYCLES_ACTIVE, CYCLES_INACTIVE);
  Led* ledD = new BlinkingCycledLed(LED_D, CYCLES_ACTIVE, CYCLES_INACTIVE);
  Potentiometer* pot = new Potentiometer(POTENTIOMETER);
  Sonar* sonar = new Sonar(TRIG, ECHO, TEMPERATURE,SONAR_TIMEOUT);
  ServoMotor* servo = new ServoMotorImpl(SERVO);
  /*    set hardware active   */
  servo->on();
  pir->calibrate(PIR_CALIBRATION_TIME);
  /*    tasks setups    */
  Task* autoMode = new AutoMode(sonar, ledA, pot, servo);
  Task* manualMode = new ManualMode(sonar, servo);
  Task* singleMode = new SingleMode(sonar, ledD, pot, pir, servo);
  StateManager* stateManager = new StateManager(manualMode, autoMode, singleMode);
  Task* checkMode = new CheckChange(SINGLE, stateManager, buttonAuto, buttonManual, buttonSingle);
  /*    scheduler setup   */
  singleMode->init(); // single has to start first
  singleMode->setActive(true);
  checkMode->init();
  checkMode->setActive(true);
  /*    add tasks to scheduler    */
  scheduler->addTask(checkMode);
  scheduler->addTask(singleMode);
  scheduler->addTask(manualMode);
  scheduler->addTask(autoMode);
  /*    timer setup   */
  Timer::setTimer(updateScheduler, INTERRUPT_TIME * TO_MICROSEC);
}

void loop() {
  scheduler->schedule();
}
void updateScheduler() {
  scheduler->update();
}
